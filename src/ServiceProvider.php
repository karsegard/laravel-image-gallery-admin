<?php

namespace KDA\ImageGallery\Admin;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasRoutes;
    use \KDA\Laravel\Traits\HasDynamicSidebar;

/* Route::crud('kdagallery', 'GalleryCrudController');
    Route::crud('kdaformat', 'FormatCrudController');
    Route::crud('kdaconversion', 'ConversionCrudController');
    Route::crud('kdaalbum', 'AlbumCrudController'); */
    protected $sidebars= [
        [
            'route'=>'kdagallery',
            'label'=> 'Galleries'
        ],
        [
            'route'=>'kdaformat',
            'label'=> 'Formats'

        ],
        [
            'route'=>'kdaconversion',
            'label'=> 'Conversions'

        ],
        [
            'route'=>'kdaalbum',
            'label'=> 'Albums'

        ]
    ];

    protected $routes = [
        'backpack/gallery.php'
    ];
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
}
