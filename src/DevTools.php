<?php

namespace  KDA\ImageGallery\Admin;


class DevTools
{

    public static function seeds(): array
    {
        return [
            'galleries',
            'conversions',
            'conversion_format',
            'conversion_gallery',
            'albums',
            'formats'
        ];
    }

    public static function sidebars(): array
    {

        return [
           

        ];
    }
}
