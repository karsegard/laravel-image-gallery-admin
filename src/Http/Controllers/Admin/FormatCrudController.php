<?php

namespace KDA\ImageGallery\Admin\Http\Controllers\Admin;

use  KDA\ImageGallery\Admin\Http\Requests\FormatRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TransportersCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class FormatCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('KDA\ImageGallery\Admin\Models\Format');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/kdaformat');
        $this->crud->setEntityNameStrings('format', 'formats');
    }

    protected function setupListOperation()
    {
        CRUD::column('desc');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(FormatRequest::class);
        CRUD::field('desc');
        CRUD::addField([
            'name' => 'operations',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name'    => 'method',
                    'type'    => 'text',
                    'label'   => 'Method',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name'    => 'value',
                    'type'    => 'text',
                    'label'   => 'Value',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

            ],
            // optional
            'new_item_label'  => 'Add operations', // customize the text of the button
            'init_rows' =>1, // number of empty rows to be initialized, by default 1
            'min_rows' => 1, // minimum rows allowed, when reached the "delete" buttons will be hidden

        ]);
        CRUD::addField([
            'name' => 'conditions',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name'    => 'conditions',
                    'type'    => 'select_from_array',
                    'options'     => [
                        'orientation'=>'Orientation',
                        'media_type'=>'MediaType',

                    ],
                    'label'   => 'Condition',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name'    => 'comparison',
                    'type'    => 'text',
                    'default'=> '=',
                    'label'   => 'comparison',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name'    => 'value',
                    'type'    => 'text',
                    'label'   => 'Valeur',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

            ],
            // optional
            'new_item_label'  => 'Add Condition', // customize the text of the button
            'init_rows' =>0, // number of empty rows to be initialized, by default 1
            'min_rows' => 0, // minimum rows allowed, when reached the "delete" buttons will be hidden

        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
