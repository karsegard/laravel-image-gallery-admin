<?php

namespace KDA\ImageGallery\Admin\Http\Controllers\Admin;

use  KDA\ImageGallery\Admin\Http\Requests\ConversionRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TransportersCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ConversionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('KDA\ImageGallery\Admin\Models\Conversion');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/kdaconversion');
        $this->crud->setEntityNameStrings('conversion', 'conversions');
    }

    protected function setupListOperation()
    {
        CRUD::column('key');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ConversionRequest::class);
        CRUD::field('key');
       
        CRUD::addField([
            'name' => 'formats',
            'type' => 'relationship',
            'attribute'=>'desc'
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
