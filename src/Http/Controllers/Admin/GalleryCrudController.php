<?php

namespace KDA\ImageGallery\Admin\Http\Controllers\Admin;

use  KDA\ImageGallery\Admin\Http\Requests\GalleryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TransportersCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class GalleryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('KDA\ImageGallery\Admin\Models\Gallery');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/kdagallery');
        $this->crud->setEntityNameStrings('galerie', 'galeries');
    }
   
    protected function setupListOperation()
    {
       CRUD::column('name');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(GalleryRequest::class);
        CRUD::field('name');
        CRUD::addField([
            'name'=>'conversions',
            'attribute'=>'key'
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
