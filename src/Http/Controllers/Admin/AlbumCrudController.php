<?php

namespace KDA\ImageGallery\Admin\Http\Controllers\Admin;

use  KDA\ImageGallery\Admin\Http\Requests\AlbumRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TransportersCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AlbumCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation  { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('KDA\ImageGallery\Admin\Models\Album');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/kdaalbum');
        $this->crud->setEntityNameStrings('album', 'albums');
    }
   
    protected function setupListOperation()
    {
        CRUD::column('name');
    }   

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(AlbumRequest::class);
        CRUD::addField([
            'name'=>'name'
        ]);
        CRUD::addField([
            'name'=>'key'
        ]);
        CRUD::addField([
            'name'=>'gallery',
            'type'=>'relationship',
            'attribute'=>'name'
        ]);
        CRUD::addField([
            'name'=>'content',
            'type'=>'browse_multiple',
            'preview'=>true,
            'sortable'   => true,
        ]);

        
    }

    protected function preview(){

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
        
    }

    public function store()
    {
        
        $response = $this->traitStore();
        // do something after save
        if($this->crud->entry){
            $entry = $this->crud->entry;
            $entry->clearImages();

            foreach($entry->content as $image){
                $entry->addImage($image);
            }
        }
        return $response;
    }


    public function update(){

        $result = $this->traitUpdate();
        if($this->crud->entry){

            $entry = $this->crud->entry;
            $entry->clearImages();
            foreach($entry->content as $image){
                $entry->addImage($image);
            }
        }
        return $result;
    }
}
