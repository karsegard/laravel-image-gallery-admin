<?php



Route::group([
    'namespace'  => 'KDA\ImageGallery\Admin\Http\Controllers\Admin',
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware()],
], function () {
    Route::crud('kdagallery', 'GalleryCrudController');
    Route::crud('kdaformat', 'FormatCrudController');
    Route::crud('kdaconversion', 'ConversionCrudController');
    Route::crud('kdaalbum', 'AlbumCrudController');

});
